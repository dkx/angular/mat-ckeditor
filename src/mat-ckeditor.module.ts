import {NgModule} from '@angular/core';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';

import {MatCKEditorComponent} from './mat-ckeditor.component';


@NgModule({
	imports: [
		CKEditorModule,
	],
	declarations: [
		MatCKEditorComponent,
	],
	exports: [
		MatCKEditorComponent,
	],
})
export class MatCKEditorModule {}
