import {AfterViewInit, Component, forwardRef, HostBinding, Input, Optional, Self, ViewChild} from '@angular/core';
import {MatFormFieldControl} from '@angular/material';
import {ControlValueAccessor, NgControl} from '@angular/forms';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {CKEditor5, CKEditorComponent} from '@ckeditor/ckeditor5-angular';
import {Subject} from 'rxjs';


@Component({
	selector: 'dkx-mat-ckeditor',
	templateUrl: './mat-ckeditor.component.html',
	providers: [
		{
			provide: MatFormFieldControl,
			useExisting: forwardRef(() => MatCKEditorComponent),
		},
	],
})
export class MatCKEditorComponent implements AfterViewInit, ControlValueAccessor
{


	private static nextId: number = 0;

	@Input('editor')
	public editorType: CKEditor5.EditorConstructor;

	@ViewChild(CKEditorComponent, {static: true})
	public editor: CKEditorComponent;

	@HostBinding()
	public readonly id: string = `dkx-mat-ckeditor-${MatCKEditorComponent.nextId++}`;

	@HostBinding('attr.aria-describedby')
	public describedBy: string = '';

	public readonly stateChanges: Subject<void> = new Subject;

	public focused: boolean = false;

	public shouldLabelFloat: boolean = true;

	private _placeholder: string = '';

	private _required: boolean = false;

	private ckeditor: CKEditor5.Editor;


	constructor(
		@Optional() @Self() public ngControl: NgControl,
	) {
		if (this.ngControl !== null) {
			this.ngControl.valueAccessor = this;
		}
	}


	public ngAfterViewInit(): void
	{
		this.editor.ready.subscribe((ckeditor) => this.ckeditor = ckeditor);
	}


	@Input()
	public get value(): string
	{
		if (this.ckeditor) {
			return this.ckeditor.getData();
		}

		return '';
	}
	public set value(value: string)
	{
		if (this.ckeditor) {
			this.stateChanges.next();
			this.ckeditor.setData(value);
		}
	}


	@Input()
	public get placeholder(): string
	{
		return this._placeholder;
	}
	public set placeholder(placeholder: string)
	{
		this._placeholder = placeholder;
		this.stateChanges.next();
	}


	@Input()
	public get required(): boolean
	{
		return this._required;
	}
	public set required(required: boolean)
	{
		this._required = coerceBooleanProperty(required);
		this.stateChanges.next();
	}


	@Input()
	public get disabled(): boolean
	{
		if (this.ckeditor) {
			this.ckeditor.isReadOnly;
		}

		return true;
	}
	public set disabled(disabled: boolean)
	{
		if (this.ckeditor) {
			this.ckeditor.isReadOnly = coerceBooleanProperty(disabled);
			this.stateChanges.next();
		}
	}


	public get empty(): boolean
	{
		return this.value === '';
	}


	public get errorState(): boolean
	{
		if (this.ngControl) {
			return this.ngControl.errors !== null;
		}

		return false;
	}


	public ngOnDestroy(): void
	{
		this.stateChanges.complete();
	}


	public setDescribedByIds(ids: string[]): void
	{
		this.describedBy = ids.join(' ');
	}


	public onContainerClick(event: MouseEvent): void
	{
		if (this.ckeditor) {
			this.ckeditor.editing.view.focus();
		}
	}


	public registerOnChange(fn: any): void
	{
		if (this.editor) {
			this.editor.registerOnChange(fn);
		}
	}


	public registerOnTouched(fn: any): void
	{
		if (this.editor) {
			this.editor.registerOnTouched(fn);
		}
	}


	public setDisabledState(isDisabled: boolean): void
	{
		if (this.editor) {
			this.editor.setDisabledState(isDisabled);
		}
	}


	public writeValue(obj: any): void
	{
		if (this.editor) {
			this.editor.writeValue(obj);
		}
	}


	public updateFocusState(state: boolean): void
	{
		if (state !== this.focused) {
			this.focused = state;
			this.stateChanges.next();
		}
	}

}
