# DKX/Angular/MatCKEditor

CKEditor for angular material

## Installation

```bash
$ npm install --save @dkx/mat-ckeditor
```

or with yarn

```bash
$ yarn add @dkx/mat-ckeditor
```

## Usage

**Module:**

```typescript
import {NgModule} from '@angular/core';
import {MatCKEditorModule} from '@dkx/mat-ckeditor';

@NgModule({
    imports: [
        MatCKEditorModule,
    ],
})
export class AppModule {}
```

**Component:**

```typescript
import {Component} from '@angular/core';
import {CKEditor5} from '@ckeditor/ckeditor5-angular';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
    templateUrl: './editor.component.html',
})
export class EditorComponent
{

    public ckeditor: CKEditor5.EditorConstructor = ClassicEditor;
    
}
```

**Template:**

```html
<dkx-mat-ckeditor [editor]="ckeditor" [formControl]="control" placeholder="Content"></dkx-mat-ckeditor>
```
